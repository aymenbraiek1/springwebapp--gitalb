package com.example.SpringWebApp.Model;

import javax.persistence.*;

@Entity
@Table(name="Publisher")
public class PubLisher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;

    public PubLisher(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public PubLisher() {
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
